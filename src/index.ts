import axios from "axios";
import * as fs from "fs/promises";

export const configureServerAndEnv = async (
  configServerUrl: string,
  path: string
) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = await axios.get(configServerUrl);
      let buf = Buffer.from(data.data.configB64Data, "base64");
      const configString = buf.toString();
      await fs.writeFile(path, configString, { flag: "w" });
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
};
