# jm-config-server-addon

## How to use

### Install

Install with yarn or npm

### General use

```typescript
try {
  await configureServerAndEnv(configServer);
} catch (err) {
  console.log(err);
  process.exit(err);
}
```

### Explicit code for api.joel.muehlena.de services

```typescript
const configServer = `http://${
  process.env.NODE_ENV === "production"
    ? "config-server.default.svc.cluster.local"
    : "192.168.183.113"
}:8081/config?filename=education:${
  process.env.NODE_ENV === "production" ? "prod" : "dev"
}`;

(async () => {
  try {
    await configureServerAndEnv(configServer, path.join(__dirname, "/.env"));
  } catch (err) {
    console.log("Error fetching config server");
  }
})();

let count = 0;
let i = setInterval(() => {
  if (count > 5000) {
    console.log("Config Timeout");
    process.exit(-1);
  }
  fs.access(path.join(__dirname, "/.env"), fs.constants.F_OK, (err) => {
    if (!err) {
      clearInterval(i);
      runApp();
    } else {
      count++;
    }
  });
}, 500);

const runApp = () => {
  //APP DATA
}
```
